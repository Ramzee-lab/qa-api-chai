# **Test Plan**

**Document Version: V1.4**

**Document Author: Matlala Irene**

**Project Sponsor: Entersekt**

**Project Team:**

**Matlala Irene: Quality Engineer**

**1.Introduction**

This Serves as the plan for testing all software features as well as reporting the results

**2.Test Plan**

Use the template below to specify the black box test cases you will run on your code. Every requirement must have a minimum of one test case. Considering equivalence class partitioning, boundary value analysis, and diabolical test cases, it is likely that each requirement should have several test cases.

| Test Case ID | Description | Expected results | Actual results | Status |
| --- | --- | --- | --- | --- |
| 1 | Verify that the Todo list items persist after browser refresh | The Todo list should items persist after browser refresh | Todo list items persist after browser refresh | Pass |
| 2 | Verify that the Todo list items are not empty | The Todo list items should not be empty | Todo list items is empty after updating it with blank spaces | Failed |
| 3 | Verify that the User is able to add Todo list | User should be able to add the Todo list | User is able to add Todo list | Pass |
| 4 | Verify that the User is able to delete Todo list | User should be able to delete the Todo list | User can delete Todo list | Pass |
| 5 | Verify that the User is able to edit Todo list | User should be able to edit the Todo list | User is able to edit the Todo list | Pass |

**3. Requirements not in Scope:**

- The Application (Front end) should be ported to Cordova and run as a mobile application
- The application back end should run in Kubernetes on 3 mode cluster with multiple replicas of each prod

**4.**  **Schedule**

The Execution estimate is from 03rd of June to 10th June 2020

**5. Risk**

Unresolved defects have huge impact on the requirements

**6. Approvals**

Quality Engineer Sign off: Irene Matlala

Business (UAT) Signoff: Kevin Da Silva

**7.Document Revision History:**

| Version | Version 1.4 |
| --- | --- |
| Name(s) | Irene |
| Date | 03rd June 2021 |