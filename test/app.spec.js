var {server} = require("../app");
var chai = require("chai");
var chaiHttp = require("chai-http");
var jsdom = require("jsdom");
const request = require('supertest');


var JSDOM = jsdom.JSDOM;
var VirtualConsole = jsdom.VirtualConsole;

var should = chai.should();
var expect = chai.expect;
chai.use(chaiHttp);

// Example unit test
describe("App", function() {
  describe("/randompage", function() {
    it("should send you to /todo", function(done) {
        request(server)
        .get("/todo")
        .end(function(err, res) {
          res.should.have.status(200);
          res.req.path.should.be.equal("/todo");
          done();
        });
    });
  });
});
describe("/add", function() {
  describe("/todo/add/", function() {
    it("Should be able to add todo items", function(done) {
      const todoItem = 'Make coffee'
        request(server)
        .post("/todo/add/")
        .send({todoItem})
        .expect(200)
        .expect( res =>{
          expect(res.body.todoItem).equal(todoItem)
        })
        .end(function(err, res) {
          done();
        });
    });
  });
});
describe("/update", function() {
  describe("/todo/edit/:id", function() {
    it("Should be able to edit todo items", function(done) {
      const todoItem = 'Make coffee'
        request(server)
        .post("/todo/edit/:id")
        .expect(200)
        .expect( res =>{
          expect(req.body).equal(todolist[req.body]);
        })
        .end(function(err, res) {
          done();
        });
    });
  });
});
