## Bugs

 - When clicking on update on todo item it clears the todo

 - The alignment of the buttons are not right

 - Todo items should not update with the trailing blank spaces

 - An empty to do item should automatically be deleted